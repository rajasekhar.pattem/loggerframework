Pod::Spec.new do |spec|

spec.name         = "RemoteLoggeriOS"
spec.version      = "1.0.0"
spec.summary      = "A short description of TestOne."
spec.description  = "Simple testing the binary framwork functionality"
spec.homepage     = "https://www.tarams.com"
spec.license      = { :type => "MIT", :file => "LICENSE" }
spec.author             = "Tarams Technologies"
spec.ios.deployment_target = "11.0"
spec.swift_versions = ['5.0']
spec.ios.vendored_frameworks = 'remote-logger-ios-framework-095d5b0364a0869d2ac542e51427bd5ec609a80b-095d5b0364a0869d2ac542e51427bd5ec609a80b/Frameworks/RemoteLoggeriOS.framework'
spec.source       = { :http => "https://gitlab.com/api/v4/projects/19252837/repository/archive.zip?sha=095d5b0364a0869d2ac542e51427bd5ec609a80b" }
spec.dependency "RealmSwift"
end
